package pages;

import config.GlobalVariable;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RegisterPage extends BasePage {

    ChromeDriver driver;
    GlobalVariable globalVariable = new GlobalVariable();

    private final By inputFirstName = By.id("firstName");
    private final By inputLastName = By.id("lastName");
    private final By inputEmail = By.id("email");
    private final By inputAddress = By.id("address");
    private final By inputPhoneNumber = By.id("phoneNumber");
    private final By inputPassword = By.id("password");
    private final By inputConfirmPassword = By.id("confirm_password");
    private final By textError = By.xpath("//label[@class='error']");
    private final By buttonRegister = By.name("register");
    private final By titlePopup = By.id("swal2-title");
    private final By contentPopup = By.id("swal2-content");
    private final By buttonOkPopup = By.xpath("//button[@class='swal2-confirm swal2-styled']");

    public RegisterPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void goToRegisterPage() {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        waitUntil(ExpectedConditions.visibilityOfElementLocated(inputFirstName));
        waitUntil(ExpectedConditions.visibilityOfElementLocated(inputLastName));
        System.out.println("Already in register page");
    }

    public void inputForRegister(String first, String last, String email, String password, String confirm) {
        setText(inputFirstName, first);
        setText(inputLastName, last);
        setText(inputEmail, email);
        setText(inputPhoneNumber, globalVariable.phoneNumber);
        setText(inputAddress, globalVariable.inputAddress);
        setText(inputPassword, password);
        setText(inputConfirmPassword, confirm);
        System.out.println("User fill in all the fields");
    }

    public void setDynamicRegister() {
        String password = globalVariable.inputPassword;
        setText(inputFirstName, globalVariable.firstName);
        setText(inputLastName, globalVariable.lastName);
        setText(inputEmail, globalVariable.inputEmail);
        setText(inputPhoneNumber, globalVariable.phoneNumber);
        setText(inputAddress, globalVariable.inputAddress);
        setText(inputPassword, password);
        setText(inputConfirmPassword, password);
        System.out.println("User fill in all the fields");
    }

    public void clickButtonRegister() {
        waitUntil(ExpectedConditions.visibilityOfElementLocated(buttonRegister));
        clickElement(buttonRegister);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        System.out.println("User do submit register");
    }

    public void verifySuccessRegister() {
        waitUntil(ExpectedConditions.visibilityOfElementLocated(titlePopup));
        String title = getText(titlePopup);
        String content = getText(contentPopup);
        clickElement(buttonOkPopup);
        Assert.assertEquals("Success", title);
        Assert.assertTrue(content.contains("Check your email"));
        System.out.println("Message" + content);
    }

    public void verifyFailedRegister() {
        if (verifyElementIsPresent(textError)) {
            List<WebElement> elements = findElements(textError);
            List<String> list = new ArrayList<>();
            for (WebElement element : elements) {
                if (!list.contains(element.getText()) && !element.getText().isEmpty()) {
                    list.add(element.getText());
                }
            }
            System.out.println("Register failed with error: " + String.join(", ", list));
            Assert.assertFalse(verifyElementNotPresent(textError));
        } else {
            waitUntil(ExpectedConditions.visibilityOfElementLocated(titlePopup));
            Assert.assertEquals("Error", getText(titlePopup));
            Assert.assertFalse(getText(contentPopup).contains("Check your email"));
            System.out.println("Register failed with error: " + getText(contentPopup));
        }
    }
}