package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.concurrent.TimeUnit;

public class LoginPage extends BasePage {

    ChromeDriver driver;

    private final By ctaToLogin = By.xpath("//a[contains(text(), 'Login')]");
    private final By inputEmail = By.id("your_email");
    private final By inputPassword = By.id("password");
    private final By buttonLogin = By.name("login");
    private final By titlePopup = By.id("swal2-title");
    private final By contentPopup = By.id("swal2-content");
    private final By buttonOkPopup = By.xpath("//button[@class='swal2-confirm swal2-styled']");
    private final By buttonLogout = By.xpath("//*[contains(text(), 'Logout')]");

    public LoginPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void goToLoginPage() {
        clickElement(ctaToLogin);
        waitUntil(ExpectedConditions.visibilityOfElementLocated(buttonLogin));
        System.out.println("Redirected to login page");
    }

    public void inputForLogin(String email, String password) {
        setText(inputEmail, email);
        setText(inputPassword, password);
        Assert.assertTrue(verifyElementIsPresent(inputEmail));
        Assert.assertTrue(verifyElementIsPresent(inputPassword));
    }

    public void clickButtonLogin() {
        clickElement(buttonLogin);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        System.out.println("User do submit login");
    }

    public void verifySuccessLogin() {
        waitUntil(ExpectedConditions.visibilityOfElementLocated(titlePopup));
        Assert.assertEquals("Success", getText(titlePopup));
        Assert.assertTrue(getText(contentPopup).contains("Welcome Back"));
        clickElement(buttonOkPopup);
        Assert.assertTrue(verifyElementIsPresent(buttonLogout));
        System.out.println("User logged in successfully");
    }

    public void verifyFailedLogin() {
        if (getText(titlePopup).equals("Error") && verifyElementIsPresent(contentPopup)) {
            System.out.println("Login failed with error: " + getText(contentPopup));
            Assert.assertFalse(getText(contentPopup).contains("Welcome Back"));
        } else {
            System.out.println("Login returned successfully");
            Assert.assertEquals("Success", getText(titlePopup));
            Assert.assertTrue(getText(contentPopup).contains("Welcome Back"));
        }
    }
}