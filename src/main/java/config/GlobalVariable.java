package config;

import com.github.javafaker.Faker;

import java.util.Locale;

public class GlobalVariable {

    Faker faker = new Faker(new Locale("in-ID"));

    public final String baseUrl = "https://qademo.onebrick.io/";
    public final String firstName = faker.name().firstName();
    public final String lastName = faker.name().lastName();
    public final String inputEmail = firstName.toLowerCase() + lastName.toLowerCase() + faker.number().digits(2) + "@brick.co";
    public final String inputPassword = faker.backToTheFuture().quote().substring(0, 8);
    public final String phoneNumber = "817" + faker.number().digits(6);
    public final String inputAddress = faker.address().fullAddress();
}