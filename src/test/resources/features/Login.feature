@login
Feature: Login
  All scenarios for login module

  @positive
  Scenario: User success login to Brick dashboard
    Given user already in login page
    When user input email "uzumakiencep@gmail.com" and password "testing123"
    And user click button login
    Then user logged in successfully

  @negative
  Scenario Outline: User failed login to Brick dashboard
    Given user already in login page
    When user input email "<email>" and password "<password>"
    And user click button login
    Then user failed to login and error found
    Examples:
      | email                   | password    |
      | uzumakiencep@gmail.com  | As4!asal    |
      | emailsaja@gmail.com     | testing123  |
      | @gmail.com              | Merauke1    |