@register
Feature: Register
  All scenarios for register module

  @positive
  Scenario: User success register to Brick dashboard
    Given user already in register page
    When user input valid registration data
    And user click button register
    Then user is registered successfully

  @negative
  Scenario Outline: User failed register to Brick dashboard
    Given user already in register page
    When user input first name "<first>", last name "<last>", email "<email>", password "<password>" and confirm password "<confirm>"
    And user click button register
    Then user failed to register and error found
    Examples:
      | first   | last    | email               | password  | confirm |
      |         | Hartono | harajuku@gmail.com  | test123   | test123 |
      | Bambang |         | harajuku@gmail.com  | test123   | test123 |
      | Bambang | Hartono |                     | test123   | test123 |
      | Bambang | Hartono | harajuku@gmail.com  |           | test123 |
      | Bambang | Hartono | harajuku@gmail.com  | test123   |         |
      | Bambang | Hartono | harajuku@           | test123   | test123 |
      | Bambang | Hartono | harajuku@gmail.com  | test1     | test1   |
      | Bambang | Hartono | harajuku@gmail.com  | test1     | test2   |
      | Bambang |         |                     |           | test1   |