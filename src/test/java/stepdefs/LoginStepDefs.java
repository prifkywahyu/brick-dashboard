package stepdefs;

import driver.InitWebDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;

public class LoginStepDefs {

    LoginPage loginPage = new LoginPage(InitWebDriver.driver);

    @Given("user already in login page")
    public void userAlreadyInLoginPage() {
        loginPage.goToLoginPage();
    }

    @When("user input email {string} and password {string}")
    public void userInputEmailAndPassword(String email, String password) {
        loginPage.inputForLogin(email, password);
    }

    @And("user click button login")
    public void userClickButtonLogin() {
        loginPage.clickButtonLogin();
    }

    @Then("user logged in successfully")
    public void userLoggedInSuccessfully() {
        loginPage.verifySuccessLogin();
    }

    @Then("user failed to login and error found")
    public void userFailedToLoginAndErrorFound() {
        loginPage.verifyFailedLogin();
    }
}