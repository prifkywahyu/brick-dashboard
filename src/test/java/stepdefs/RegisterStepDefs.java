package stepdefs;

import driver.InitWebDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.RegisterPage;

public class RegisterStepDefs {

    RegisterPage registerPage = new RegisterPage(InitWebDriver.driver);

    @Given("user already in register page")
    public void userAlreadyInRegisterPage() {
        registerPage.goToRegisterPage();
    }

    @When("user input valid registration data")
    public void userInputValidRegistrationData() {
        registerPage.setDynamicRegister();
    }

    @When("user input first name {string}, last name {string}, email {string}, password {string} and confirm password {string}")
    public void userInputFirstNameLastNameEmailPasswordAndConfirmPassword(String first, String last, String email, String password, String confirm) {
        registerPage.inputForRegister(first, last, email, password, confirm);
    }

    @And("user click button register")
    public void userClickButtonRegister() {
        registerPage.clickButtonRegister();
    }

    @Then("user is registered successfully")
    public void userIsRegisteredSuccessfully() {
        registerPage.verifySuccessRegister();
    }

    @Then("user failed to register and error found")
    public void userFailedToRegisterAndErrorFound() {
        registerPage.verifyFailedRegister();
    }
}