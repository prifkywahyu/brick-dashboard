# brick-dashboard

Repository for Brick technical test submission.

## Pre-requisite
General:
```
Java SDK
IDEA (Intellij or etc)
Cucumber for Java
```

Web automation:
```
Selenium library
```

## Get Started
All scenarios:
```
Go to InitTestRunner class
Click play
```

Each scenarios:
```
Go to each feature file
Click play in your preferred scenario
```